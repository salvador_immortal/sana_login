from django.db import models
from datetime import date,datetime,timedelta
# Create your models here.

class DeviceId(models.Model):
    device_token = models.CharField(default='',max_length=256,blank=True)
    is_restricted = models.BooleanField(default=False)
    restrict_untill = models.DateTimeField(auto_now_add=False,auto_now=False,default=datetime.now)

    def check_restricted(self):
        from cuser.models import User
        from active_codes.models import ActiveCodes 
        if self.is_restricted and self.restrict_untill.timestamp()<datetime.now().timestamp():
            self.is_restricted = False
            self.save()
            return True
        elif self.is_restricted and self.restrict_untill.timestamp()>datetime.now().timestamp():
            return False
        else:
            register_counter = User.objects.filter(device=self).count()
            active_code_counter = ActiveCodes.objects.filter(user__device=self).count()
            if (register_counter!=0 and active_code_counter!=0) and (register_counter%6==0 or active_code_counter%6==0):
                self.is_restricted = True
                self.restrict_untill = datetime.now() + timedelta(hours=1)
                self.save()
                User.objects.filter(device=self).update(is_restricted = True,
                                                        restrict_untill = datetime.now() + timedelta(hours=1))
                return False
            return True
            # self.is_restricted = True
            # self.restrict_untill = datetime.now() + timedelta(hours=1)
            # self.save()
            # return False