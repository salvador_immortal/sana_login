from django.shortcuts import render
from django.db.models import Q,F
from django.contrib.auth import authenticate
# from django.http import JsonResponse
from rest_framework.generics import CreateAPIView
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
import random
import hashlib

from django.contrib import auth

from .models import User
from device_identifier.models import DeviceId
from active_codes.models import ActiveCodes
from datetime import date,datetime,timedelta
import json
# Create your views here.


class UserCreate(CreateAPIView):
    renderer_classes = (JSONRenderer,)
    parser_classes = (JSONParser,)

    def post(self, request ,*args, **kwargs):
        instance = request.data
        code = random.randint(999, 10000)
        # token = hashlib.sha256().hexdigest()
        try:
            device = DeviceId.objects.get(
                device_token=instance['div_token'],
            )
        except:
            device = DeviceId.objects.create(
                device_token=instance['div_token'],
            )
        if not device.check_restricted():
            return Response({'state':False,'msg':'your device has been restricted'})  
        try:
            user_id = User.objects.create(
                        phone = instance['phone'],
                        device=device,
                        restrict_untill = datetime.now()
            )
            ActiveCodes.objects.create(
                user = user_id,
                code=code,
            )
            return Response({'state':True,'code':code})  
        except:
            return Response({'state':False,'msg':'UniqueErr'})

class UserActivate(CreateAPIView):
    renderer_classes = (JSONRenderer,)
    parser_classes = (JSONParser,)

    def post(self, request ,*args, **kwargs):
        instance = request.data
        try:
            ActiveCodes.objects.get(user__phone=instance['phone'],code=instance['code'])
        except:
            return Response({'state':False,'msg':'Code Or Phone is Wrong'})
        user = User.objects.get(phone=instance['phone'])
        if instance['password']:
            user.set_password(instance['password'])
            user.is_active = True
            user.save()
            return Response({'state':True,'msg':'You Activated'})
        else:
            return Response({'state':False,'msg':'password cant be empty'})

class UserLogin(CreateAPIView):
    renderer_classes = (JSONRenderer,)
    parser_classes = (JSONParser,)

    def post(self, request ,*args, **kwargs):
        instance = request.data
        if not instance['phone'] or not instance['password']:
            return Response({'state':False,'msg':'Please provide both phone and password'})
        temp_user = ''
        try:
            temp_user = User.objects.get(phone=instance['phone'])
        except:
            pass
        user = authenticate(phone=instance['phone'], password=instance['password'])
        if not user:
            if temp_user and temp_user.check_login_restriction():
                return Response({'state':False,'msg': 'Your Login Restricted'})
            return Response({'state':False,'msg': 'Invalid Credentials Or Ypur Account is not active'})
        if temp_user.check_login_restriction():
                return Response({'state':False,'msg': 'Your Login Restricted'})
        token, _ = Token.objects.get_or_create(user=user)
        return Response({'state':True,'token': token.key})

class UserLogout(CreateAPIView):
    renderer_classes = (JSONRenderer,)
    parser_classes = (JSONParser,)

    def post(self, request ,*args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')
        try:
            Token.objects.filter(key=token).delete()
            return Response({'state':True,'msg': 'you logged out'})
        except:
            return Response({'state':True,'msg': 'you logged out'})

class UserCkeckLogin(CreateAPIView):
    renderer_classes = (JSONRenderer,)
    parser_classes = (JSONParser,)

    def post(self, request ,*args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')
        try:
            Token.objects.get(key=token).delete()
            return Response({'state':True,'msg': 'you are loggin'})
        except:
            return Response({'state':False,'msg': 'you logged out'})

