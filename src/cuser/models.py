from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from device_identifier.models import DeviceId
# from active_codes.models import ActiveCodes
from datetime import date,datetime,timedelta
from .managers import UserManager
# Create your models here.

class User(AbstractBaseUser, PermissionsMixin):
    phone = models.CharField(max_length=11,unique=True)
    fullname = models.CharField(max_length=60,default='',blank=True)
    password = models.CharField(default='',max_length=256,blank=True)
    device = models.ForeignKey(DeviceId)
    token = models.CharField(default='',max_length=256,blank=True)
    is_active = models.BooleanField(default=False)
    is_restricted = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    restrict_untill = models.DateTimeField(auto_now_add=False,auto_now=False,default=datetime.now)
    login_wrong_pass = models.IntegerField(default=0)
    login_restrict = models.BooleanField(default=False)
    login_restrict_untill = models.DateTimeField(auto_now_add=False,auto_now=False,default=datetime.now)

    objects = UserManager()

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []


    def check_active(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def check_restricted(self):
        if not self.is_admin:
            if self.is_restricted and self.restrict_untill.timestamp()<datetime.now().timestamp():
                self.is_restricted = False
                self.save()
                return True
            elif self.is_restricted and self.restrict_untill.timestamp()>datetime.now().timestamp():
                return False
            else:
                self.is_restricted = True
                self.restrict_untill = datetime.now() + timedelta(hours=1)
                self.save()
                return False
        return True
    
    def check_login_restriction(self):

        if self.login_restrict and self.login_restrict_untill.timestamp()<datetime.now().timestamp():
            self.login_restrict = True
            self.login_wrong_pass = self.login_wrong_pass + 1
            self.save()
            if self.login_wrong_pass!=0  and self.login_wrong_pass%3==0:
                self.login_restrict = True
                self.login_restrict_untill = datetime.now() + timedelta(hours=1)
                self.save()
                return True
            return False
        elif self.login_restrict and self.login_restrict_untill.timestamp()>datetime.now().timestamp():
            return True
        else:
            if self.login_wrong_pass!=0  and self.login_wrong_pass%3==0:
                self.login_restrict = True
                self.login_restrict_untill = datetime.now() + timedelta(hours=1)
                self.save()
                return True

            # self.login_restrict = True
            self.login_wrong_pass = self.login_wrong_pass + 1
            # self.login_restrict_untill = datetime.now() + timedelta(hours=1)
            self.save()
            return False
        # if self.login_restrict and self.login_restrict_untill.timestamp()>datetime.now().timestamp():
        #         self.login_wrong_pass = self.login_wrong_pass + 1
        #         if self.login_wrong_pass%3==0:
        #             self.login_restrict = True
        #             self.login_restrict_untill = datetime.now() + timedelta(hours=1)
        #         self.save()
        #         return True
        # return False

    def get_full_name(self):
        # The user is identified by their email address
        return self.phone

    def get_short_name(self):
        # The user is identified by their email address
        return self.phone

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin
    
# @receiver(pre_save, sender=User)
# def pre_save_user(sender, instance, **kwargs):
#     raise ValueError('hey')
#     # if not instance._state.adding:
#         # counter = User.objects.filter(device=instance.device).count()
#         # print (counter)
#     # else:
#     #     counter = User.objects.filter(device=instance.device).count()
#     #     print (counter)
#     from active_codes.models import ActiveCodes
#     register_counter = User.objects.filter(device=instance.device).count()
#     active_code_counter = ActiveCodes.objects.filter(user__device=instance.device).count()
#     if register_counter%6==0 or active_code_counter%6==0:
#         device = DeviceId.objects.get(pk=instance.device)
#         device.is_restricted = True
#         device.restrict_untill = datetime.now() + timedelta(hours=1)
#         device.save()
